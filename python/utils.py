from dotenv import load_dotenv
import shutil
import os


def push_lighthouse_chaperone():
    # copy from local to network
    load_dotenv('.env')

    shutil.copyfile(os.environ.get("LIGHTHOUSE_DB_LOCAL"), os.environ.get("LIGHTHOUSE_DB_NETWORK"))
    print('Pushed Lighthouse Data')
    shutil.copyfile(os.environ.get("CHAPERONE_LOCAL"), os.environ.get("CHAPERONE_NETWORK"))
    print('Pushed Chaperone Data')


def pull_lighthouse_chaperone():
    # copy from network to local
    load_dotenv('.env')

    shutil.copyfile(os.environ.get("LIGHTHOUSE_DB_NETWORK"), os.environ.get("LIGHTHOUSE_DB_LOCAL"))
    print('Pulled Lighthouse Data')
    shutil.copyfile(os.environ.get("CHAPERONE_NETWORK"), os.environ.get("CHAPERONE_LOCAL"))
    print('Pulled Chaperone Data')