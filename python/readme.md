# Instructions

Ensure that the `.env` file has the correct paths for your use case.

## PUSH

To push, run
```
python3 push_steamvr_data.py
```

## PULL

To pull, run
```
python3 pull_steamvr_data.py
```